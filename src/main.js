// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import routes from './routes/routes'
import App from './App'
import 'vuetify/dist/vuetify.min.css'
import Vuetify from 'vuetify'
import 'material-design-icons/iconfont/material-icons.css'

import sectionsService from './services/sections'
import themesService from './services/themes'
import materialsService from './services/materials'
import questionsService from './services/questions'
import answersService from './services/answers'
import breadcrumbsService from './services/breadcrumbs'
import testsService from './services/tests'

Vue.use(VueRouter)
Vue.use(Vuetify)
Vue.use(Vuex)

const router = new VueRouter({
  mode: 'history',
  routes: routes,
  linkActiveClass: 'active'
})

const store = new Vuex.Store({
  state: {
    breadcrumbs: [],
    sections: [],
    themes: [],
    questions: [],
    answers: [],
    testQuestion: {},
    testResponse: {}
  },
  mutations: {
    setBreadcrumbs (state, breadcrumbs) {
      state.breadcrumbs = breadcrumbs
    },

    setTestQuestion (state, metadata) {
      state.testQuestion = metadata
    },
    setTestResponse (state, metadata) {
      state.testResponse = metadata
    },

    setSections (state, sections) {
      state.sections = sections
    },
    addSection (state, section) {
      state.sections.push(section)
    },
    updateSection (state, section) {
      Vue.set(state.sections, state.sections.findIndex(a => a.id === section.id), section)
    },
    deleteSection (state, id) {
      Vue.delete(state.sections, state.sections.findIndex(a => a.id === id))
    },

    setThemes (state, themes) {
      state.themes = themes
    },
    addTheme (state, theme) {
      state.themes.push(theme)
    },
    updateTheme (state, theme) {
      Vue.set(state.themes, state.themes.findIndex(a => a.id === theme.id), theme)
    },
    deleteTheme (state, id) {
      Vue.delete(state.themes, state.themes.findIndex(a => a.id === id))
    },

    setMaterials (state, materials) {
      state.materials = materials
    },
    addMaterial (state, material) {
      state.materials.push(material)
    },
    updateMaterial (state, material) {
      Vue.set(state.materials, state.materials.findIndex(a => a.id === material.id), material)
    },
    deleteMaterial (state, id) {
      Vue.delete(state.materials, state.materials.findIndex(a => a.id === id))
    },

    setQuestions (state, questions) {
      state.questions = questions
    },
    addQuestion (state, question) {
      state.questions.push(question)
    },
    updateQuestion (state, question) {
      Vue.set(state.questions, state.questions.findIndex(a => a.id === question.id), question)
    },
    deleteQuestion (state, id) {
      Vue.delete(state.questions, state.questions.findIndex(a => a.id === id))
    },

    setAnswers (state, answers) {
      state.answers = answers
    },
    addAnswer (state, answer) {
      state.answers.push(answer)
    },
    updateAnswer (state, answer) {
      Vue.set(state.answers, state.answers.findIndex(a => a.id === answer.id), answer)
    },
    deleteAnswer (state, id) {
      Vue.delete(state.answers, state.answers.findIndex(a => a.id === id))
    }
  },
  actions: {
    getTestQuestion ({commit}, sectionId) {
      testsService.question(sectionId)
        .then(res => {
          commit('setTestQuestion', res.data.metadata)
        })
    },
    postTestAnswer ({commit, dispatch}, {sectionId, answer, callback}) {
      testsService.answer(sectionId, answer)
        .then(res => {
          commit('setTestResponse', res.data)
          callback()
          if (res.data.should_change) {
            setTimeout(() => {
              dispatch('getTestQuestion', sectionId)
            }, 6000)
          }
        })
    },

    getBreadcrumbs ({commit}, {klass, id}) {
      breadcrumbsService.index(klass, id)
        .then(res => {
          commit('setBreadcrumbs', res.data)
        })
    },

    getSections ({commit}) {
      sectionsService.index()
        .then(res => {
          commit('setSections', res.data)
        })
    },
    addSection ({commit}, {payload}) {
      sectionsService.post(payload)
        .then(res => {
          commit('addSection', res.data)
        })
    },
    updateSection ({commit}, {id, payload}) {
      sectionsService.patch(id, payload)
        .then(res => {
          commit('updateSection', res.data)
        })
    },
    deleteSection ({commit}, id) {
      sectionsService.delete(id)
        .then(() => {
          commit('deleteSection', id)
        })
    },

    getThemes ({commit}, sectionId) {
      sectionsService.show(sectionId)
        .then(res => {
          commit('setThemes', res.data.themes)
        })
    },
    addTheme ({commit}, {sectionId, payload}) {
      themesService.post(sectionId, payload)
        .then(res => {
          commit('addTheme', res.data)
        })
    },
    updateTheme ({commit}, {id, payload}) {
      themesService.patch(id, payload)
        .then(res => {
          commit('updateTheme', res.data)
        })
    },
    deleteTheme ({commit}, id) {
      themesService.delete(id)
        .then(() => {
          commit('deleteTheme', id)
        })
    },

    getMaterials ({commit}, sectionId) {
      sectionsService.show(sectionId)
        .then(res => {
          commit('setMaterials', res.data.materials)
        })
    },
    addMaterial ({commit}, {sectionId, payload}) {
      materialsService.post(sectionId, payload)
        .then(res => {
          commit('addMaterial', res.data)
        })
    },
    updateMaterial ({commit}, {id, payload}) {
      materialsService.patch(id, payload)
        .then(res => {
          commit('updateMaterial', res.data)
        })
    },
    deleteMaterial ({commit}, id) {
      materialsService.delete(id)
        .then(() => {
          commit('deleteMaterial', id)
        })
    },

    getQuestions ({commit}, themeId) {
      themesService.show(themeId)
        .then(res => {
          commit('setQuestions', res.data.questions)
        })
    },
    addQuestion ({commit}, {themeId, payload}) {
      questionsService.post(themeId, payload)
        .then(res => {
          commit('addQuestion', res.data)
        })
    },
    updateQuestion ({commit}, {id, payload}) {
      questionsService.patch(id, payload)
        .then(res => {
          commit('updateQuestion', res.data)
        })
    },
    deleteQuestion ({commit}, id) {
      questionsService.delete(id)
        .then(() => {
          commit('deleteQuestion', id)
        })
    },

    getAnswers ({commit}, questionId) {
      questionsService.show(questionId)
        .then(res => {
          commit('setAnswers', res.data.answers)
        })
    },
    addAnswer ({commit}, {questionId, payload}) {
      answersService.post(questionId, payload)
        .then(res => {
          commit('addAnswer', res.data)
        })
    },
    updateAnswer ({commit}, {id, payload}) {
      answersService.patch(id, payload)
        .then(res => {
          commit('updateAnswer', res.data)
        })
    },
    deleteAnswer ({commit}, id) {
      answersService.delete(id)
        .then(() => {
          commit('deleteAnswer', id)
        })
    }

  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  template: '<App/>',
  components: {App},
  router: router,
  store: store
})
