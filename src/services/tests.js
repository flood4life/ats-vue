import api from './api'

export default {
  question: (sectionId) => api.get('/tests/question', {params: {section_id: sectionId}}),

  answer: (sectionId, answer) => api.post('/tests/answer', {section_id: sectionId, answer: answer})
}
