import api from './api'

export default {
  show: (id) => api.get(`/answers/${id}`),

  post: (questionId, payload) => api.post(`/questions/${questionId}/answers`, payload),

  patch: (id, payload) => api.patch(`/answers/${id}`, payload),

  delete: (id) => api.delete(`/answers/${id}`)
}
