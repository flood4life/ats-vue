import api from './api'

export default {
  show: (id) => api.get(`/themes/${id}`),

  post: (sectionId, payload) => api.post(`/sections/${sectionId}/themes`, payload),

  patch: (id, payload) => api.patch(`/themes/${id}`, payload),

  delete: (id) => api.delete(`/themes/${id}`)
}
