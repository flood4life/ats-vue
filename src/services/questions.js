import api from './api'

export default {
  show: (id) => api.get(`/questions/${id}`),

  post: (themeId, payload) => api.post(`/themes/${themeId}/questions`, payload),

  patch: (id, payload) => api.patch(`/questions/${id}`, payload),

  delete: (id) => api.delete(`/questions/${id}`)
}
