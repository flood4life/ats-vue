import axios from 'axios'

const baseURL = (process.env.NODE_ENV === 'production' ? 'https://ats-f4l.herokuapp.com/api' : 'http://lvh.me:3000/api')

export default axios.create({
  baseURL: baseURL,
  timeout: 25000
})
