import api from './api'

export default {
  show: (id) => api.get(`/materials/${id}`),

  post: (sectionId, payload) => api.post(`/sections/${sectionId}/materials`, payload),

  patch: (id, payload) => api.patch(`/materials/${id}`, payload),

  delete: (id) => api.delete(`/materials/${id}`)
}
