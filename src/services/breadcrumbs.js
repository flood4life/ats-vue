import api from './api'

export default {
  index: (klass, id) => api.get('/breadcrumbs', {params: {klass: klass, id: id}})
}
