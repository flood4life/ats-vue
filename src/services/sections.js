import api from './api'

export default {
  index: () => api.get('/sections'),

  show: (id) => api.get(`/sections/${id}`),

  post: (payload) => api.post('/sections', payload),

  patch: (id, payload) => api.patch(`/sections/${id}`, payload),

  delete: (id) => api.delete(`/sections/${id}`)
}
