import Course from '../components/Course.vue'
import AdminCourse from '../components/admin/Course.vue'
import Section from '../components/Section.vue'
import AdminSection from '../components/admin/Section.vue'
import AdminTheme from '../components/admin/Theme.vue'
import AdminQuestion from '../components/admin/Question.vue'

export default [
  {
    path: '/', redirect: '/sections'
  },
  {
    path: '/sections',
    component: Course,
    name: 'studentCourse'
  },
  {
    path: '/sections/:sectionId',
    component: Section,
    name: 'studentSection'
  },
  {
    path: '/admin',
    component: AdminCourse,
    name: 'adminCourse'
  },
  {
    path: '/admin/sections/:sectionId',
    component: AdminSection,
    name: 'adminSection'
  },
  {
    path: '/admin/themes/:themeId',
    component: AdminTheme,
    name: 'adminTheme'
  },
  {
    path: '/admin/questions/:questionId',
    component: AdminQuestion,
    name: 'adminQuestion'
  }

]
